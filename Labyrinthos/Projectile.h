/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include <SFML/Graphics.hpp>

class Projectile : public sf::Drawable, public sf::Transformable {
public:
	Projectile(sf::Vector2f startingPosition, sf::Vector2f direction);
	void update(float dt);
	sf::Vector2f getPosition();

private:
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	sf::CircleShape shape;
	sf::Vector2f direction;
	const float shapeRadius = 5.f;
	const float speed = 800.f;
};