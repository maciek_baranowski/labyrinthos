/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#include "Auxiliary.h"

sf::Vector2f aux::normalize(sf::Vector2f vec) {
	return vec / sqrt(vec.x*vec.x + vec.y*vec.y);
}

float aux::length(sf::Vector2f vec) {
	return sqrt(vec.x*vec.x + vec.y*vec.y);
}