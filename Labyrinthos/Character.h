/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include <SFML/Graphics.hpp>

class Character : public sf::Drawable, public sf::Transformable {
public:
	void setPosition(sf::Vector2f);
	sf::Vector2f getPosition() const;
	void setRotation(float angle);
	virtual void update(float dt) = 0;
	float getRadius() const;

protected:
	float shapeRadius;
	float spriteScale;
	float textureRadius;

	sf::Vector2f position;
	sf::Texture texture;
	sf::Sprite sprite;
};