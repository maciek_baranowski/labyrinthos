/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#include "Enemy.h"
#include "Auxiliary.h"

Enemy::Enemy(const Labyrinth::array& lab) :
destination(-1.f, -1.f),
attackCooldownTimer(0.f),
labyrinth(lab)
{
	spriteScale = 1.f;
	textureRadius = 24.f; //update every time enemy's texture is changed or use 48x48 textures
	shapeRadius = textureRadius * spriteScale;
	sprite.setPosition(sf::Vector2f(0.f, 0.f));
	if (!texture.loadFromFile("Images/enemy.png", sf::IntRect(0, int(textureRadius) * 2 * 3, 
		int(textureRadius) * 2, int(textureRadius) * 2)))
	{
		printf("Error loading player texture\n");
	}
	sprite.setTexture(texture);
	sprite.setScale(sf::Vector2f(spriteScale, spriteScale));

	//setting origin to center of the shape, so rotation is performed
	//around it instead of top-left corner
	sprite.setOrigin(sf::Vector2f(textureRadius, textureRadius));

	HP = startingHP;
}

void Enemy::update(float dt) {
	sf::Vector2f rotVector = getPosition();
	if (aux::length(destination - getPosition()) < .2f)
		destination = sf::Vector2f(-1.f, -1.f);
	if (destination.x > 0.f)
		setPosition(getPosition() + aux::normalize(destination - getPosition()) * speed * dt);

	attackCooldownTimer = (attackCooldownTimer - dt < 0.f) ? 0.f : attackCooldownTimer - dt;

	//look where you are going based on position change during this update
	rotVector -= getPosition();
	if (aux::length(rotVector) > 0.05f)
		sprite.setRotation(atan2(rotVector.y, rotVector.x) * 180.f / aux::PI + textureRotation);
}

void Enemy::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(sprite, states);
}

unsigned int Enemy::getHP() const {
	return HP;
}

unsigned int Enemy::decreaseHP(unsigned int amount) {
	HP = amount >= HP ? 0 : HP - amount;
	return HP;
}

void Enemy::setDestination(sf::Vector2f dest) {
	destination = dest;
}

bool Enemy::attackIfYouCan(const std::shared_ptr<Character> target) {
	if ((aux::length(getPosition() - target->getPosition()) < meleeRange + getRadius() +
		target->getRadius()) && (attackCooldownTimer < .05f)) { //can attack
		attackCooldownTimer = attackCooldown;
		return true;
	}
	return false;
}