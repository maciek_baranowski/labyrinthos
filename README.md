# *LABYRINTHOS* #

**What is it:**
Labyrinthos is a top-down shooter taking place in randomly generated labyrinths. The only objective of the game is to shoot all the enemies in the maze and don't get killed yourself. Project is written using SFML library ( http://www.sfml-dev.org/ ).

**License:**
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl> .
All source code is available under GNU GPL 3.0 license ( http://www.gnu.org/licenses/gpl-3.0.txt ).

**How to set up:**
You have to have Microsoft Visual Studio 2015 to build this project. Simply clone this repository to your hard drive and build it using VS.

**How to play:**
Use WASD to move and Mouse Left Button to shoot. In case of dying or eliminating all enemies, press Q to start a new session.