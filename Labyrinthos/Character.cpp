/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#include "Character.h"

void Character::setPosition(sf::Vector2f newPosition) {
	sprite.setPosition(newPosition);
}

sf::Vector2f Character::getPosition() const {
	return sprite.getPosition();
}

void Character::setRotation(float angle) {
	sprite.setRotation(angle);
}

float Character::getRadius() const {
	return shapeRadius;
}