/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#include "Player.h"
#include "Auxiliary.h"

Player::Player() :
direction(0.f, 0.f),
scratchTextureRadius(24.f),
currentHP(healthPoints)
{
	spriteScale = .5f;
	textureRadius = 50.f; //update every time player's texture is changed or use 100x100 textures
	shapeRadius = spriteScale * textureRadius;
	sprite.setPosition(sf::Vector2f(0.f, 0.f));
	if (!texture.loadFromFile("Images/player.png"))
	{
		printf("Error loading player texture\n");
	}
	if (!scratchTexture.loadFromFile("Images/enemy.png", sf::IntRect(int(scratchTextureRadius) * 2 * 2,
		int(scratchTextureRadius) * 2 * 7, int(scratchTextureRadius) * 2, 
		int(scratchTextureRadius) * 2)))
	{
		printf("Error loading scratch texture\n");
	}
	sprite.setTexture(texture);
	sprite.setScale(sf::Vector2f(spriteScale, spriteScale));
	scratchSprite.setTexture(scratchTexture);

	//setting origin to center of the shape, so rotation is performed
	//around it instead of top-left corner
	sprite.setOrigin(sf::Vector2f(textureRadius, textureRadius));
	scratchSprite.setOrigin(sf::Vector2f(scratchTextureRadius, scratchTextureRadius));
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(sprite, states);
	if (scratchCooldownTimer > .05f)
		target.draw(scratchSprite, states);
}


void Player::update(float dt) {
	if (aux::length(direction) > .1f)
		setPosition(sprite.getPosition() + aux::normalize(direction) * speed);

	scratchSprite.setPosition(getPosition());
	scratchCooldownTimer = (scratchCooldownTimer - dt < 0.f) ? 0.f : scratchCooldownTimer - dt;
	setRotation(desiredRotation);
}

std::shared_ptr<Projectile> Player::primaryFire(sf::Vector2f mouseLocation) {
	sf::Vector2f normalizedDestination = aux::normalize(mouseLocation - getPosition());
	return gun->primaryFire(sprite.getPosition() + shapeRadius * normalizedDestination,
		normalizedDestination);
}

void Player::gotAttacked(sf::Vector2f source) {
	scratchCooldownTimer = scratchCooldown;
	source -= getPosition();
	scratchSprite.setRotation(atan2(source.y, source.x) * 180.f / aux::PI + scratchTextureRotation);

	if (currentHP > 0)
		currentHP--;
}

int Player::getCurrentHP() const {
	return currentHP;
}

void Player::setDesiredRotation(float rotation) {
	desiredRotation = rotation;
}

void Player::resetHP() {
	currentHP = healthPoints;
}