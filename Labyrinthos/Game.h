/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include <SFML\Graphics.hpp>
#include <memory>
#include <vector>
#include <set>

#include "Player.h"
#include "Enemy.h"
#include "Config.h"
#include "Labyrinth.h"

class Game {
public:
	Game();

	void run();

private:
	void startNewSession();
	void processEvents();
	void handlePressedKey(sf::Event& event);
	void handleReleasedKey(sf::Event& event);
	void handleMouseMovement(sf::Event& event);
	void update();
	void render();
	void adjustGameView();
	void checkAndFixCollisionsBetweenCharacters();
	void checkIfEnemiesSeePlayer();

	void generateLabyrinth();
	sf::Vector2f checkMovement();
	bool isOnScreen(sf::Vector2f position, float radius);

private:
	enum Direction {
		RIGHT = 0,
		UP = 1,
		LEFT = 2,
		DOWN = 3
	};
	std::array<bool, 4> directionPressedKey;

	// Main game clock
	sf::Clock mainClock;

	sf::RenderWindow gameWindow;
	sf::View gameView;
	std::shared_ptr<Player> player;

	sf::RectangleShape tempPassage;

	float timeAccumulator;
	float timeDT;
	sf::Vector2f previousState;
	sf::Vector2f currentState;
	sf::Text winDefeatText;
	sf::Font font;

	Labyrinth::array labyrinth;
	std::set<std::shared_ptr<Enemy>> enemies;
	std::set<std::shared_ptr<Projectile>> projectiles;
};