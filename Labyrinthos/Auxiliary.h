/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include <SFML/Graphics.hpp>

namespace aux {

	static const float PI = 3.14159265f;
	static const float SQRT_2 = sqrt(2.f);

	//Returns a normalized vector from sf::Vector2f
	sf::Vector2f normalize(sf::Vector2f vec);

	//length of given sf::vector2f
	float length(sf::Vector2f vec);
}