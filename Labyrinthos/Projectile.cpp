/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#include "Projectile.h"
#include "Auxiliary.h"

Projectile::Projectile(sf::Vector2f startingPosition, sf::Vector2f direction) :
direction(direction),
shape()
{
	shape.setOrigin(sf::Vector2f(shapeRadius, shapeRadius) / 2.f);
	shape.setFillColor(sf::Color::Red);
	shape.setPosition(startingPosition - shape.getOrigin());
	shape.setRadius(shapeRadius);
}

void Projectile::update(float dt) {
	shape.setPosition(shape.getPosition() + dt * speed * direction);
}

void Projectile::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(shape, states);
}

sf::Vector2f Projectile::getPosition() {
	return shape.getPosition() + shape.getOrigin();
}