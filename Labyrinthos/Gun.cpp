/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#include "Gun.h"

Gun::Gun() :
damage(50)
{

}

std::shared_ptr<Projectile> Gun::primaryFire(sf::Vector2f startingPosition,
	sf::Vector2f direction) {
	return std::shared_ptr<Projectile>(new Projectile(startingPosition, direction));
}