/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include "Character.h"
#include "Labyrinth.h"
#include <memory>

class Enemy : public Character {
public:
	Enemy(const Labyrinth::array& labyrinth);
	void update(float dt);
	unsigned int getHP() const;
	unsigned int decreaseHP(unsigned int amount = 1);
	void setDestination(sf::Vector2f destination);
	bool attackIfYouCan(const std::shared_ptr<Character> target);

private:
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
	unsigned int HP;
	const unsigned int startingHP = 4;
	const float speed = 60.f;
	sf::Vector2f destination;
	const float attackCooldown = 2.f;
	const float meleeRange = 5.f;
	float attackCooldownTimer;
	const float textureRotation = 90.f;
	const Labyrinth::array& labyrinth;
};