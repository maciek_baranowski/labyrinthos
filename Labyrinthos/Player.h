/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include <SFML/Graphics.hpp>
#include <array>
#include "Character.h"
#include "Gun.h"

class Player : public Character {
public:
	Player();
	void update(float dt);
	sf::Vector2f direction;
	std::shared_ptr<Projectile> primaryFire(sf::Vector2f mouseLocation);
	void gotAttacked(sf::Vector2f source);
	int getCurrentHP() const;
	void resetHP();
	void setDesiredRotation(float rotation);

private:
	const float speed = 6.f;
	std::unique_ptr<Gun> gun;

	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	sf::Sprite scratchSprite;
	sf::Texture scratchTexture;
	float scratchTextureRadius;
	const float scratchTextureRotation = 90.f;

	const float scratchCooldown = 1.5f;
	float scratchCooldownTimer;

	const int healthPoints = 5;
	int currentHP;

	float desiredRotation;
};