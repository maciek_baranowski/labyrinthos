/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include "Projectile.h"
#include <memory>

class Gun {
public:
	Gun();
	std::shared_ptr<Projectile> primaryFire(sf::Vector2f startingPosition,
		sf::Vector2f direction);

private:
	unsigned int damage;
};