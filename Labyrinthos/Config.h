/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include <string>

namespace cfg {
	static const int WINDOW_WIDTH = 800;
	static const int WINDOW_HEIGHT = 600;
	static const std::string WINDOW_TITLE = "Labyrinthos";

	static const int LABYRINTH_WIDTH = 15;
	static const int LABYRINTH_HEIGHT = 10;

	static const float CELL_SIZE = 200.f;

	static const int CELLS_PER_WINDOW_HEIGHT = WINDOW_HEIGHT /
		static_cast<int>(CELL_SIZE) + 1;
	static const int CELLS_PER_WINDOW_WIDTH = WINDOW_WIDTH /
		static_cast<int>(CELL_SIZE) + 1;
}