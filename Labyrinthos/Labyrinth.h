/*
Copyright (C) 2015 Maciej Baranowski <maciek.baranowski@yahoo.pl>
This piece of code is part of Labyrinthos project.
Full source code is available at https://bitbucket.org/maciek_baranowski/labyrinthos/
under GNU GPL 3.0 license.
License is available at http://www.gnu.org/licenses/gpl-3.0.txt
*/

#pragma once

#include "Config.h"
#include <array>

namespace Labyrinth {
	enum cellState {
		NOT_YET_CONSIDERED,
		PASSAGE,
		WALL,
		LOCKED_WALL
	};

	typedef std::array<std::array<cellState, cfg::LABYRINTH_HEIGHT>,
		cfg::LABYRINTH_WIDTH> array;
}